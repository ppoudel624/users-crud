import axios from "axios";

export const base = () => {
  const data = {
    baseURL: "https://jsonplaceholder.typicode.com",
    headers: {
      "content-type": "application/json",
    },
  };
  return axios.create(data);
};

//get list of all users
export const get_all_users = async () => {
  return await base()
    .get("/users")
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error;
    });
};

//delete user
export const delete_all_users = async (id) => {
  return await base()
    .delete("/users/" + id)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error;
    });
};

//edit user
export const edit_user = async (id, name, phone, email, website) => {
  return await base()
    .put(
      "/users/" + id,
      JSON.stringify({
        name: name,
        phone: phone,
        website: website,
        email: email,
      })
    )
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error;
    });
};
