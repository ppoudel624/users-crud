import '../styles/globals.css'
import '../styles/usercard.scss'

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
