import React, { useState } from "react";
import { Button, Modal } from "antd";
export default function DeleteModal({
  open,
  loading,
  setOpen,
  isDelete,
  deleteUser,
}) {
  const handleCancel = () => {
    setOpen(false);
  };
  return (
    <div>
      <Modal
        open={open}
        title="Are you sure you want to delete?"
        onOk={deleteUser}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            No
          </Button>,
          !loading ? (
            <Button key="submit" type="primary" onClick={deleteUser}>
              Yes
            </Button>
          ) : (
            <Button loading key="submit" type="primary" onClick={deleteUser}>
              Deleting
            </Button>
          ),
        ]}
      ></Modal>
    </div>
  );
}
