import {
  EditOutlined,
  DeleteOutlined,
  HeartOutlined,
  HeartFilled,
  MailOutlined,
  PhoneOutlined,
  GlobalOutlined,
} from "@ant-design/icons";
import { Card, Modal, Button, Form, Input, Row, Col } from "antd";
import { useEffect, useState, useRef } from "react";
import {
  get_all_users,
  delete_all_users,
  edit_user,
} from "../pages/api/api.js";
import avatar from "../assets/download.png";
import Image from "next/image";
import DeleteModal from "./DeleteModal";

export default function UserInfo() {
  //all the states are defined here
  const [userList, setUserList] = useState("");
  const [editname, setEditName] = useState("");
  const [editnumber, setEditnumber] = useState("");
  const [editemail, setEditemail] = useState("");
  const [editwebsite, setEditwebsite] = useState("");
  const [editId, setEditid] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isDelete, setIsDelete] = useState(false);
  const [open, setOpen] = useState(false);
  const [deleteid, setDeleteId] = useState("");
  const [isLoading, setisLoading] = useState(false);
  const [favorite, setisFavorite] = useState([]);
  const [checked, setisChecked] = useState(false);

  //add/remove from favorite
  const favoriteButton = (id) => {
    if (favorite.includes(id)) {
      setisFavorite(
        favorite.filter((item) => {
          return item !== id;
        })
      );
    } else {
      setisFavorite([...favorite, id]);
    }
  };

  //editing modal
  const showModal = (val) => {
    setEditid(val?.id);
    setEditName(val?.name);
    setEditemail(val?.email);
    setEditnumber(val?.phone);
    setEditwebsite(val?.website);
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  //fetching all users
  useEffect(() => {
    async function fetchData() {
      const response = await get_all_users();
      setUserList(response?.data);
      console.log(response);
    }
    fetchData();
  }, []);

  //deleting users

  const showDeleteModal = (val) => {
    setOpen(true);
    setDeleteId(val);
  };
  const deleteUser = async () => {
    console.log("del", deleteid);
    setisLoading(true);
    const response = await delete_all_users(deleteid);
    if (response.status === 200) {
      setisLoading(false);
      setIsDelete(true);
      setOpen(false);
      setUserList(
        userList.filter((users) => {
          return users?.id !== deleteid;
        })
      );
    }
  };

  return (
    <>
      <EditModal
        isLoading={isLoading}
        setisLoading={setisLoading}
        userList={userList}
        setUserList={setUserList}
        id={editId}
        setEditName={setEditName}
        setEditemail={setEditemail}
        setEditnumber={setEditnumber}
        setEditwebsite={setEditwebsite}
        editnumber={editnumber}
        editwebsite={editwebsite}
        editemail={editemail}
        editname={editname}
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        handleCancel={handleCancel}
        handleOk={handleOk}
      />
      <DeleteModal
        loading={isLoading}
        open={open}
        setOpen={setOpen}
        isDelete={isDelete}
        deleteUser={deleteUser}
      />
      <div className="usercard">
        <Row gutter={24}>
          {Object.entries(userList).map(([key, value]) => {
            return (
              <Col xl={6} lg={8} md={8} xs={24} sm={12}>
                <Card
                  size="small"
                  style={{ width: 300, marginTop: "20px" }}
                  cover={
                    <Image
                      alt="example"
                      style={{ width: "200px", margin: "auto" }}
                      height={200}
                      src={avatar}
                    />
                  }
                  actions={[
                    !favorite.includes(value?.id) ? (
                      <HeartOutlined
                        onClick={() => favoriteButton(value?.id)}
                        style={{
                          fontSize: "20px",
                          color: "red",
                        }}
                        key="like"
                      />
                    ) : (
                      <HeartFilled
                        onClick={() => favoriteButton(value?.id)}
                        style={{ fontSize: "20px", color: "red" }}
                        key="like"
                      />
                    ),

                    <EditOutlined
                      onClick={() => showModal(value)}
                      style={{ fontSize: "20px" }}
                      key="edit"
                    />,
                    <DeleteOutlined
                      onClick={() => showDeleteModal(value?.id)}
                      style={{ fontSize: "20px" }}
                      key="delete"
                    />,
                  ]}
                >
                  <span className="user-name">{value?.name}</span>
                  <br />
                  <span className="user-details">
                    <MailOutlined />
                    {value?.email}
                  </span>
                  <br />
                  <span className="user-details">
                    <PhoneOutlined />
                    {value?.phone}
                  </span>
                  <br />
                  <span className="user-details">
                    <GlobalOutlined />
                    {value?.website}
                  </span>
                </Card>
              </Col>
            );
          })}
        </Row>
      </div>
    </>
  );
}

export function EditModal({
  id,
  isLoading,
  setisLoading,
  userList,
  setUserList,
  isModalOpen,
  setEditName,
  setEditemail,
  setEditnumber,
  setEditwebsite,
  editname,
  editnumber,
  editemail,
  editwebsite,
  setIsModalOpen,
  handleCancel,
  handleOk,
}) {
  const formRef = useRef(null);

  useEffect(() => {
    formRef.current?.setFieldsValue({
      username: editname,
      phone: editnumber,
      website: editwebsite,
      email: editemail,
    });
  }, [editname.editnumber, editemail, editwebsite]);

  const onFinish = async (values) => {
    setIsModalOpen(true);
    setisLoading(true);
    const response = await edit_user(
      id,
      editname,
      editnumber,
      editemail,
      editwebsite
    );

    if (response?.status === 200) {
      setIsModalOpen(false);
      setisLoading(false);
      const updatedUsers = userList.map((user) => {
        if (user.id === id) {
          user.name = editname;
          user.phone = editnumber;
          user.email = editemail;
          user.website = editwebsite;
        }

        return user;
      });

      setUserList((userList) => updatedUsers);
    }
  };

  console.log("item", editname);
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      <Modal
        title="Edit User"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
          isLoading ? (
            <Button loading key="submit" type="primary" onClick={onFinish}>
              Saving
            </Button>
          ) : (
            <Button key="submit" type="primary" onClick={onFinish}>
              Save
            </Button>
          ),
        ]}
      >
        <Form
          ref={formRef}
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Name"
            name="username"
            onChange={(e) => setEditName(e.target.value)}
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            onChange={(e) => setEditemail(e.target.value)}
            rules={[
              {
                required: true,
                message: "Please input your email!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Phone"
            name="phone"
            type="number"
            onChange={(e) => setEditnumber(e.target.value)}
            rules={[
              {
                required: true,
                message: "Please input your phone !",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Website"
            name="website"
            onChange={(e) => setEditwebsite(e.target.value)}
            rules={[
              {
                required: true,
                message: "Please input your website!",
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
